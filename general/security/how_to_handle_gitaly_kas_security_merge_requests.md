## How to handle security merge requests from Gitaly and KAS

Security issues from Gitaly and KAS are rare and not automatically embedded in the patch
release process. The process to include them is composed of two sections:

1. The security fix prepared on the Gitaly or KAS security repository and,
2. The merge request that bumps the version file on the GitLab Security repository.

The first part is prepared by the stage team (Gitaly or KAS), the second part requires manual intervention
by Release Managers, this last part aims to be automated by
[performing the Gitaly update task during the Patch Release].

Once the Gitaly or KAS security merge requests are ready:

1. Merge the security merge request targeting `master`.
2. Disable the automatic [Gitaly update task] or the KAS update task.
3. Bump the `GITALY_VERSION` or the `GITLAB_KAS_VERSION` on the GitLab security repository by using the merge sha
from the security merge request (see examples: [Gitaly example], [KAS example])
4. Prior to merging the backports, ensure the merge request that bumps the version has been
deployed to GitLab.com.

After the patch release is published, the Gitaly update task should be re-enabled automatically.

[performing the Gitaly update task during the Patch Release]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1236
[Gitaly example]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2176
[Gitaly update task]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/112/edit
[KAS example]: https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4063
