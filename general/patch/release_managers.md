# Release Manager runbook for patch releases

Planned patch releases are scheduled on the second and fourth Wednesdays of the month, around
the release week. The patch release schedule accommodates the GitLab SLO/SLAs:

* [Release SLO]
* [Bug remediation SLO]
* [Vulnerability remediation SLA]

The [release dashboard] provides an overview of the next patch release information. Consider the
patch release schedule is best effort and, as long as the schedule meets the GitLab SLO/SLAs, it can be updated
at the discreation of release managers.

Release managers will perform two patch releases during their shift, if required
they can also performed [unplanned critical patches] or [patch releases for a single version].

## Planned patch releases.

This is the default patch release process that is performed according to the [GitLab Maintenance Policy], 
includes bug fixes for the current GitLab version and security fixes for the current and the two previous
GitLab versions.

The [overview of the planned patch release] explains the end-to-end process.

### Process

The patch release will include all the bug fixes merged into the stable branches and the security fixes that are
ready at the given time. To perform a planned patch release

1. Two days before the patch release date, usually on Monday, release managers trigger the ChatOps command
on the `#f_upcoming_release` Slack channel

```
/chatops run release prepare --security 
```

2. A release task issue will be created on the [release task tracker] with the required steps to tag, deploy and
publish the patch release, the steps are described below. Release Managers are the DRIs of this issue and it is
their responsibility to see it through and update it as they progress.
3. Two days before the due date, Release Managers will:
   1. Disable the automated security implementation issue processor to prevent new security issues from being
    automatically linked to the Security Tracking issue. This processor can be disabled at any time by deactivating the `security-target issue processor` [pipeline schedule].
   2. Merge security merge requests targeting the default branch with:

   ```text
   # On Slack
   /chatops run release merge --security --default-branch
   ```

   Security merge requests targeting the default branch will only be merged if:

   * They belong to GitLab Security
   * They are associated to security issues linked to the current Security Tracking Issue.
   * They are associated to security issues that are ready to be processed.

   Other tasks including dealing with other components which may have security
   picks for the target release as well, such as
   [Gitaly merge requests] and GitLab-Pages. Each of these are handled manually.

   Once the security merge requests have been merged, Canonical and Security default branches will diverge,
   [security merge-train pipeline schedule] will deal with this divergence by updating Security default branch
   based on the Canonical default branch content, see [merge train] for more info.

   Security issues that are linked to the Security Tracking issue less than 24 hours
   before the due date will be automatically unlinked by the Release Tools Bot. For
   example, if the due date of the Security Tracking issue is 28th August, security
   issues added after 27th August 00:00 UTC will be automatically unlinked.

   However, security issues that are linked as blockers ('is blocked by' vs
   'relates to') to the Security Tracking issue will not be unlinked.
   This allows high priority security issues to be linked to the tracking issue
   close to the due date.

   The automatic unlinking behavior is behind a feature flag, which can be disabled
   if required: [unlink_late_security_issues]

   Release-tools will generate and post a comment with a table to the security tracking issue. The
   table shows the status of every security issue linked to the security tracking issue.
   Example table: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1589#note_1055523334.

   Do not edit the table, except to add comments under the "Release manager comments" column. Changes to
   any other column will be overwritten any time a new issue is automatically linked or unlinked or
   the next time the `/chatops run release merge --security` command is executed
3. One day before the due date, Release Managers will merge GitLab security backports and
security merge requests associated with other satellite GitLab projects.
   ```
   # On Slack
   /chatops run release merge --security
   ```
4. A blog post will be automatically created on the [www-gitlab-com security] repository by the
security pipeline referenced on the release task issue. The blog post will include the bug fixes
and the security fixes that were processed. AppSec is the DRI of the blog post content.
5. On the due date of the Patch release, Release Managers will tag, publish the packages and
complete the Patch Release.

### Considerations

1. When dealing with security fixes, it is important that all the
work happens **only** on Security repositories so nothing is disclosed publicly before we intend it.
1. Any delay needs to be escalated to Sr. Manager of Infrastructure. We normally try to
avoid releasing security packages to the community on Friday an during worldwide low availability periods
an during worldwide low availability periods in order to avoid situations where a customer may be exploited
during the weekend.

## Unplanned critical patch release

[Unplanned critical patch releases] are ad-hoc processes coordinated by AppSec used to quickly mitigate a
high-severity vulnerability in order to meet the [Vulnerability remediation SLA].

**It is important to note that "critical" does not mean that the release needs to
happen the very moment it is requested, because [planned patch releases] are SLO-driven,
highly consider if the next schedule patch release can be used to release the high-severity vulnerability**.

### Process

From a release manager standpoint, a critical patch release requires drafting a release plan with a
timeline in addition to tasks defined for [planned patch releases].

A few questions that you can discuss with your fellow release manager:

* Are the fixes ready for issue(s) that prompted this critical release?
  * If no, find out when the fixes will be ready for the issue and *all backport* versions.
  * If yes, are all backports ready? If yes, this means that release managers have everything they need to work on the release.
* Where are we currently in the release cycle?
  * If you are working on the latest monthly release:
    * Will working on patch release endanger the monthly release? Is it
      possible to work on RCs and all backports for the patch release without
      breaching the deadline?
    * If you prepare a patch release and it gets postponed for some reason,
      will creating all other RCs as patch releases cause more work?

When you get answers for the questions above, start working on the timeline for
the critical release.

An example of how a release timeline could look like, when a critical release
is called for during the latest monthly release:

```text
To meet the deadline of the release date and not block the regular release, proposing a schedule:

* All fixes and backports need to be ready for merge by the end of Monday.
* On Tuesday, @release-manager-1 will merge all security fixes into their respective branches,
  and deploy the security fixes to our different environments.
* On Tuesday, @release-manager-2 will ensure the deployment makes it to production and tag the release.
* On Wednesday, @release-manager-1 will publish the packages and wrap up the rest of the patch release tasks.
```

The plan can be more detailed to include more people with specific tasks.
Aim to `@`-mention people responsible for specific tasks, to avoid the
[bystander effect](https://en.wikipedia.org/wiki/Bystander_effect).

Once the plan is finalized it is important to stick with the plan. You should refuse any changes
with the items that need to be included in the release if they put the release deadlines at risk.
You can consider being flexible if there is enough time to recover from failure
(CI failing, deploy goes wrong, etc.), but in most cases you will need to stick strictly to the
plan to make sure that the deadlines given to the Security team are respected.

Once the plan is defined, Release Managers can create the release tasks issue with
the following command and follow the steps listed there.

To trigger a critical unplanned patch release:

```
/chatops run release prepare --security --critical
```

A blog post will be automatically created by this process or it can also be manually prepared by AppSec.

## Patch release for a single version

If required, patch releases can be prepared for a single version. To prepare a patch release for the current version
execute the following command on the `#f_upcoming_release` Slack  channel:

```
/chatops run release prepare
```

Or alternatively, to prepare a patch release for a specific version:

```
/chatops run release prepare 16.11.3
```

The command will generate a blog post with the unreleased merge requests for the single version on the [www-gitlab-com] repository.

# Utilities

- [List of bug fixes waiting to be released]
- [Stable branch configuration]


[Bug remediation SLO]: https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity-slos
[Managed Versioning]: components/managed-versioning/index.md#components-under-managed-versioning
[Merge request approval rules]: https://gitlab.com/gitlab-org/gitlab/-/settings/merge_requests
[merge train]: general/security/utilities/merge_train.md
[GitLab Maintenance Policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[Gitaly merge requests]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_kas_security_merge_requests.md
[List of bug fixes waiting to be released]: ./bug_fixes_waiting_to_be_released.md
[overview of the planned patch release]: ./readme.md#planned-patch-release
[patch releases for a single version]: #patch-release-for-a-single-version
[planned patch releases]: #planned-patch-release
[pipeline schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[Protected branch rules]: https://gitlab.com/gitlab-org/gitlab/-/settings/repository
[release managers]: https://about.gitlab.com/community/release-managers/
[Release SLO]: https://internal.gitlab.com/handbook/company/performance-indicators/product/saas-platforms-section/#deliveryreleases---mean-time-between-security-releases
[release dashboard]: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1
[release task tracker]: https://gitlab.com/gitlab-org/release/tasks/-/issues
[security merge-train pipeline schedule]: https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules?scope=inactive
[Stable branch configuration]: ./stable_branch_configuration.md
[unlink_late_security_issues]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags/203/edit
[unplanned critical patches]: #unplanned-critical-patch-release
[Unplanned critical patch releases]: ./readme.md#unplanned-critical-patch-release
[Vulnerability remediation SLA]: https://handbook.gitlab.com/handbook/security/threat-management/vulnerability-management/#remediation-slas
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
[www-gitlab-com security]: https://gitlab.com/gitlab-org/security/www-gitlab-com
[security release deadlines]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md#security-release-deadlines
