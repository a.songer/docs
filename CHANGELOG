# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

## Added

- 2023-07-27 - [Security release pipeline section](general/secrurity/release-manager.md) for release manager reference (!610)
- 2023-07-28 - Added a runbook for [deploying and old package](runbooks/deploy-an-old-package.md) (!611)
- 2023-08-09 - Added a glossary for deployments, packages and rollout strategies (!615)
- 2023-08-16 - Added an overview of releases.yml (!620)
- 2023-08-22 - Added patcher practice to the [monthly release issue](https://gitlab.com/gitlab-org/release-tools/-/merge_requests/2582)
- 2023-10-09 - Procedure for mitigating bugs introduced by security merge request [documentation](general/security/bugs_introduced_by_security_merge_request.md) and [runbook](general/security/runbooks/revert-security-merge-request.md)
- 2023-10-26 - Added a guide for troubleshooting Delivery-metrics issues
- 2023-11-15 - Updated Resolving QA failures runbook noting automatic release/task issue creation for QA failures.
- 2023-11-07 - Used [`generate-toc-file.sh` script](./scripts/generate-toc-file.sh) to generate README.md file for document directories. Ran as a part of the pipeline to validate.
- 2023-02-15 - Added guide for [Clickhouse](general/clickhouse/index.md)

## Changed

- 2023-08-07 - Updated [security to canonical syncing runbook](general/security/how_to_sync_security_with_canonical.md) to cover the new permissions process if direct push is needed (!616)
- 2023-10-28 - Updated [QA failures runbook](runbooks/resolving-qa-failures.md) to make process easier to follow (!654)
- 2023-11-28 - Updated [Overview of post deploy migration pipeline](general/post_deploy_migration/readme.md) to add information about troubleshooting the "No pending post migrations available" message (!668)
- 2023-12-11 - Moved all security runbooks to [./runbooks/security/](./runbooks/security/)

## Fixed
