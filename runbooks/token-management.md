# Token management

Token management should follow the [company guidelines for automation and access
tokens](https://about.gitlab.com/handbook/engineering/automation/).

If you need a token from a project or a bot owned by the delivery group,
please [file an access token
request](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/new?issuable_template=access_token_request)
in our issue tracker.

The issue template will guide you on collecting the needed information
and will serve as [tracker for all the tokens we issued](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/?sort=updated_desc&state=all&label_name%5B%5D=access-token).
